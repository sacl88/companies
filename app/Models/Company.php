<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    protected $fillable = [
        'name', 'email', 'logo',
    ];

    protected $guarded = [ 'id' ];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function delete()
    {
        $delete = DB::transaction(function () {
            $this->employees()->delete();
            $deleteParent = parent::delete();
            return $deleteParent;
        });

        return $delete;
    }
}
