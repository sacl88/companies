<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'name', 'last_name', 'company_id', 'email', 'phone'
    ];

    protected $guarded = [ 'id' ];

    public function company()
    {
        /** @var TYPE_NAME $this */
        return $this->belongsTo(Company::class);
    }
}
