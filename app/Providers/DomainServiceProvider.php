<?php

namespace App\Providers;

use App\Domain\Repositories\EmployeeRepository;
use App\Repositories\EloquentCompanyRepository;
use App\Repositories\EloquentEmployeeRepository;
use Illuminate\Support\ServiceProvider;
use App\Domain\Repositories\CompanyRepository;

class DomainServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->bindingRepositories();
    }

    private function bindingRepositories()
    {
        $this->app->bind(CompanyRepository::class, EloquentCompanyRepository::class);
        $this->app->bind(EmployeeRepository::class, EloquentEmployeeRepository::class);
    }
}
