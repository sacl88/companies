<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'email' => Rule::unique('employees')->ignore($this->route('employee')),
            'company_id' => 'required|numeric|exists:companies,id',
            'phone' => 'required|numeric',
        ];
    }
}
