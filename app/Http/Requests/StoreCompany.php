<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'logo' => Rule::dimensions()->minWidth(100)->minHeight(100),
            'email' => Rule::unique('companies')->ignore($this->route('company'))
        ];
    }

    public function messages()
    {
        return [
            'logo.dimensions' => 'The logo has invalid image dimensions Must be from 100 x 100',
        ];
    }
}
