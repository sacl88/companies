<?php

namespace App\Http\Controllers;

use App\Domain\Services\CompanyService;
use App\Domain\Services\EmployeeService;
use App\Http\Requests\StoreEmployee;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class EmployeeController extends Controller
{
    private $employeeService;
    private $companyService;

    public function __construct(
        EmployeeService $employeeService,
        CompanyService $companyService
    )
    {
        $this->employeeService = $employeeService;
        $this->companyService = $companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->employeeService->all();
        return view('employees.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = $this->companyService->all();
        return view('employees.create')->with('companies', $companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        $request->validated();
        $this->employeeService->create($request->all());
        return Redirect::to('employees')
            ->with('message', trans('view.messages.success', ['action' => trans('view.messages.actions.store')]));
    }

    /**
     * Display the specified resource.
     *
     * @param Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        if (!$employee) {
            return Redirect::to('employees')->with('error', trans('views.messages.error_not_found'));
        }

        return view('employees.show')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        if (!$employee) {
            return Redirect::to('employees')
                ->with('error', trans('views.messages.error_not_found'));
        }
        $companies = $this->companyService->all();
        return view('employees.edit')->with('employee', $employee)->with('companies', $companies);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEmployee $request, Employee $employee)
    {
        if (!$employee) {
            return Redirect::to('employees')->with('error', trans('views.messages.error_not_found'));
        }
        $request->validated();
        $this->employeeService->update($request->all(), $employee->id);
        return Redirect::to('employees')
            ->with('message', trans('view.messages.success', ['action' => trans('view.messages.actions.update')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employee $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Employee $employee)
    {
        if (!$employee) {
            return Redirect::to('employees')
                ->with('error', trans('views.messages.error_not_found'));
        }

        $employee = $this->employeeService->delete($employee->id);
        return Redirect::to('employees')
            ->with('message', trans('view.messages.success', ['action' => trans('view.messages.actions.delete')]));
    }
}
