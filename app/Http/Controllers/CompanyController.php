<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCompany;
use App\Models\Company;
use Illuminate\Support\Facades\Redirect;
use Image;
use Illuminate\Http\Request;
use App\Domain\Services\CompanyService;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    private $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->companyService->all();
        return view('companies.index')->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompany $request)
    {
        $request->validated();
        $requestData = $this->validateAndSaveImage($request);
        $requestData = ($requestData) ? $requestData : $request->all();
        $this->companyService->create($requestData);
        return Redirect::to('companies')
            ->with('message', trans('view.messages.success', ['action' => trans('view.messages.actions.store')]));
    }

    /**
     * Display the specified resource.
     *
     * @param Company $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Company $company)
    {
        if (!$company) {
            return Redirect::to('companies')->with('error', trans('views.messages.error_not_found'));
        }

        return view('companies.show')->with('company', $company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        if (!$company) {
            return Redirect::to('companies')
                ->with('error', trans('views.messages.error_not_found'));
        }

        return view('companies.edit')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreCompany $request
     * @param Company $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreCompany $request, Company $company)
    {
        if (!$company) {
            return Redirect::to('companies')->with('error', trans('views.messages.error_not_found'));
        }
        $request->validated();
        $requestData = $this->validateAndSaveImage($request);
        $requestData = ($requestData) ? $requestData : $request->all();
        $this->companyService->update($requestData, $company->id);
        return Redirect::to('companies')
            ->with('message', trans('view.messages.success', ['action' => trans('view.messages.actions.update')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Company $company)
    {
        if (!$company) {
            return Redirect::to('companies')
                ->with('error', trans('views.messages.error_not_found'));
        }

        $company = $this->companyService->delete($company->id);
        return Redirect::to('companies')
            ->with('message', trans('view.messages.success', ['action' => trans('view.messages.actions.delete')]));
    }

    private function validateAndSaveImage($request)
    {
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $fileName = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(120, 120, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('/public/' . $fileName, $img, 'public');
            $requestData = $request->all();
            $requestData['logo'] = $fileName;
            return $requestData;
        }

        return false;
    }
}
