<?php

namespace App\Repositories;

use App\Domain\Repositories\CompanyRepository;
use App\Models\Company;


class EloquentCompanyRepository implements CompanyRepository
{
    private $db;

    public function __construct(Company $model)
    {
        $this->db = $model;
    }

    /**
     * @param array $parameters
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($parameters = array())
    {
        return $this->db->with('employees')->paginate(10);
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function create($parameters = array())
    {
        $company = $this->db->create($parameters);

        return $this->db->where("id", $company->id)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $company = $this->db->find("company.id", $id)->first();

        if (!is_null($company) && $company->count() == 0) {

            return null;
        }

        return $company;
    }

    /**
     * @param array $parameters
     * @param $id
     * @return null
     */
    public function update($parameters = array(), $id)
    {
        $company = $this->db->find($id);

        if (!$company) {
            return null;
        }

        $company->update($parameters);

        return $company;
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $company = $this->db->find($id);

        if (is_null($company)) {
            return null;
        }

        return $company->delete();
    }

}
