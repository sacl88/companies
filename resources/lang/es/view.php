<?php

return [
    "companies" => [
        "form" => [
            'title_form' => 'Crear nueva compañía',
            'title_error' => 'El formulario tiene errores',
            'name' => 'nombre',
            'email' => 'correo electrónico',
            'logo' => 'Imagen para el logo'
        ]
    ],

    "employees" => [
        "form" => [
            'title_form' => 'Crear nuevo empleado',
            'title_error' => 'El formulario tiene errores',
            'name' => 'Nombre',
            'last_name' => 'Apellido',
            'email' => 'correo electrónico',
            'name_company' => 'Nombre de la compañía',
            'phone' => 'Teléfono'
        ]
    ],

    'messages' => [
        'success' => 'Recurso :action correctamente',
        'actions' => [
            'delete' => 'eliminado',
            'store' => 'creado',
            'update' => 'actualizado',
        ],
        'error_not_found' => 'El recurso no existe'
    ]
];
