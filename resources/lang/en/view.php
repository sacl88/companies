<?php

return [
    "companies" => [
        "form" => [
            'title_form' => 'Create new company',
            'title_error' => 'The form have errors',
            'name' => 'name',
            'email' => 'Email address',
            'logo' => 'Image logo'
        ]
    ],

    "employees" => [
        "form" => [
            'title_form' => 'Create new employee',
            'title_error' => 'The form have errors',
            'name' => 'Name',
            'last_name' => 'Last name',
            'email' => 'Email address',
            'name_company' => 'Company name',
            'phone' => 'Phone'
        ]
    ],

    'messages' => [
        'success' => 'Resource :action successfully',
        'actions' => [
            'delete' => 'deleted',
            'store' => 'created',
            'update' => 'updated',
        ],
        'error_not_found' => 'The resource does not exist'
    ]
];
