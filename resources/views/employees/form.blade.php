<div class="form-group">
    <label for="exampleInputName" class="required">@lang('view.employees.form.name')</label>
    <input type="text" class="form-control" id="exampleInputName" name="name" placeholder="Enter name"
           value="{{old('name', $employee->name ?? '')}}" required>
</div>
<div class="form-group">
    <label for="exampleInputLastName" class="required">@lang('view.employees.form.last_name')</label>
    <input type="text" class="form-control" id="exampleInputLastName" name="last_name" placeholder="Enter last name"
           value="{{old('last_name', $employee->last_name ?? '')}}" required>
</div>
<div class="form-group">
    <label for="exampleInputEmail1">@lang('view.employees.form.email')</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email"
           value="{{old('email', $employee->email ?? '')}}">
</div>
<div class="form-group">
    <label for="exampleInputCompany">@lang('view.employees.form.name_company')</label>
    <select class="form-control" name="company_id" id="exampleInputCompany">
        @if(isset($companies) && count($companies) > 0)
            @foreach($companies as $company)
                @if(isset($employee))
                    <option {{$selected = ($company->id == $employee->company->id) ? 'selected="selected"' : ''}} value='{{ $company->id }}'>{{ $company->name }}</option>
                @else
                    <option value='{{ $company->id }}'>{{ $company->name }}</option>
                @endif
            @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label for="exampleInputPhone">@lang('view.employees.form.phone')</label>
    <input type="number" class="form-control" id="exampleInputPhone" name="phone" placeholder="Enter phone"
           value="{{old('phone', $employee->phone ?? '')}}">
</div>
