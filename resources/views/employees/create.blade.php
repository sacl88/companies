@extends("theme.$theme.layout")

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('includes.form-error')
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3>@lang('view.employees.form.title_form')</h3>
                </div>
                <div class="box-body">
                    <form role="form" action="{{route('employees.store')}}" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="box-body">
                            @include('employees.form')
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            @include('includes.btn-form-create')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
