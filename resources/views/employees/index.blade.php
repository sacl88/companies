@extends("theme.$theme.layout")

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('includes.message')
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3>List Employees</h3> <a href="{{url('employees/create')}}" class="btn btn-success">Create new
                        employee</a>
                </div>
                <div class="box-body">
                    @if(isset($employees) && count($employees) > 0)
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>phone</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($employees as $key => $employee)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$employee->name}}</td>
                                    <td>{{$employee->last_name}}</td>
                                    <td>{{$employee->email}}</td>
                                    <td>{{$employee->company->name}}</td>
                                    <td>{{$employee->phone}}</td>
                                    <td>
                                        <form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
                                            <a href="{{url('employees/' . $employee->id , 'edit')}}"
                                               class="btn btn-primary">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $employees->links() }}
                    @else
                        Empty data
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
