@extends("theme.$theme.layout")

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3>Dashboard</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
