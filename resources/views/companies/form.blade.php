<div class="form-group">
    <label for="exampleInputName" class="required">@lang('view.companies.form.name')</label>
    <input type="text" class="form-control" id="exampleInputName" name="name" placeholder="Enter name" value="{{old('name', $company->name ?? '')}}" required>
</div>
<div class="form-group">
    <label for="exampleInputEmail1">@lang('view.companies.form.email')</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email" value="{{old('email', $company->email ?? '')}}">
</div>
<div class="form-group">
    <label for="exampleInputFile">@lang('view.companies.form.logo')</label>
    <input type="file" name="logo" id="exampleInputFile">

    <p class="help-block">with 100 x 100.</p>
</div>
