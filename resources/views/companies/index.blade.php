@extends("theme.$theme.layout")

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('includes.message')
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3>List Companies</h3> <a href="{{url('companies/create')}}" class="btn btn-success">Create new
                        company</a>
                </div>
                <div class="box-body">
                    @if(isset($companies) && count($companies) > 0)
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th style="width: 150px">Logo</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($companies as $key => $company)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$company->name}}</td>
                                    <td>
                                        {{$company->email}}
                                    </td>
                                    <td>
                                        @if($company->logo)
                                            <img src="{{ asset('storage/' . $company->logo) }}">
                                        @else
                                            Sin logo
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('companies.destroy',$company->id) }}" method="POST">
                                            <a href="{{url('companies/' . $company->id , 'edit')}}"
                                               class="btn btn-primary">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $companies->links() }}
                    @else
                        Empty data
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
