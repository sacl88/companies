## requirements
    apache, nginx, wamp, lamp server,
    mysql 5.6 or 5.7
    php 7. > 1
## Instructions install
- Adminlte views must not be included in the repository, in this case it was included to fail installation

- first git clone repository in your server.

- make a copy of the configuration file

        cp .env.example .env
        
- create the database and add the settings in the .env file by adding the username and name of the database
- Install dependencies

        composer install
        
- be sure to give permissions the following directories

        chmod 775 -R bootstrap/
        
        chmod 775 -R storage/
        
- run the migrations and the user seeder

        php artisan migrate
        php artisan db:seed
        
- Generate key to project

       php artisan key:generate
        
- public images link with command 
    
        php artisan storage:link
        
- make sure that all unit tests are passing correctly showing all in green, with the following command:
        
        vendor/bin/phpunit --testdox
        
- We make sure to clear cache and so on
    
        php artisan optimize:clear
        
- we run the project

        php artisan serve
       
- We enter the url http://127.0.0.1:8000 and enter with user admin@admin.com pass: password and enjoy!
