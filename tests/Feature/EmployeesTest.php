<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Employee;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeesTst extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function canSeeListEmployees()
    {
        $this->withExceptionHandling();

        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);

        $employee = Employee::create([
            'name' => 'Name employee',
            'last_name' => 'Lats name employee',
            'company_id' => $company->id,
            'email' => 'employee@gmail.com',
            'phone' => 31458888
        ]);

        $response = $this->get('/employees');
        $response->assertStatus(200);
        $this->assertEquals(1, Employee::count());
    }

    /** @test */
    public function canSeeCreateViewEmployees()
    {
        $this->withExceptionHandling();
        $response = $this->get('/employees/create');
        $response->assertStatus(200);
    }

    /** @test */
    public function canCreateEmployee()
    {
        $this->withExceptionHandling();
        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);
        $response = $this->post('/employees', [
            "name" => "employee",
            "last_name" => "employee",
            "email" => "sergioleon1127@gmail.com",
            "company_id" => $company->id,
            "phone" => "3145552"
        ]);
        $response->assertStatus(302);
        $this->assertEquals(1, Employee::count());
    }

    /** @test */
    public function canShowViewEditCompanies()
    {
        $this->withExceptionHandling();
        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);

        $employee = Employee::create([
            'name' => 'Name employee',
            'last_name' => 'Lats name employee',
            'company_id' => $company->id,
            'email' => 'employee@gmail.com',
            'phone' => 31458888
        ]);

        $response = $this->get('/employees/' . $employee->id . '/edit');
        $response->assertStatus(200);
    }

    /** @test */
    public function canUpdateEmployee()
    {
        $this->withExceptionHandling();

        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);

        $employee = Employee::create([
            'name' => 'Name employee',
            'last_name' => 'Lats name employee',
            'company_id' => $company->id,
            'email' => 'employee@gmail.com',
            'phone' => 31458888
        ]);

        $response = $this->put('/employees/' . $employee->id, [
            'name' => 'Name employee lorem',
            'last_name' => 'Lats name employee lorem',
            'company_id' => $company->id,
            'email' => 'employee2@gmail.com',
            'phone' => 31458888
        ]);

        $this->assertDatabaseHas('employees', [
            'name' => 'Name employee lorem',
            'last_name' => 'Lats name employee lorem',
            'company_id' => $company->id,
            'email' => 'employee2@gmail.com',
            'phone' => 31458888
        ]);
        $response->assertStatus(302);
        $this->assertEquals(1, Employee::count());
    }

    /** @test */
    public function canDeleteEmployee()
    {
        $this->withExceptionHandling();

        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);

        $employee = Employee::create([
            'name' => 'Name employee',
            'last_name' => 'Lats name employee',
            'company_id' => $company->id,
            'email' => 'employee@gmail.com',
            'phone' => 31458888
        ]);

        $response = $this->delete('/employees/' . $employee->id);
        $response->assertStatus(302);
        $this->assertEquals(0, Employee::count());
    }
}
