<?php

namespace Tests\Feature;

use App\Models\Company;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompaniesTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function canSeeAListCompanies()
    {
        $this->withExceptionHandling();
        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);

        $response = $this->get('/companies');
        $response->assertStatus(200);
        $this->assertEquals(1, Company::count());
    }

    /** @test */
    public function canSeeCreateViewCompanies()
    {
        $this->withExceptionHandling();
        $response = $this->get('/companies/create');
        $response->assertStatus(200);
    }

    /** @test */
    public function canShowViewEditCompanies()
    {
        $this->withExceptionHandling();
        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);

        $response = $this->get('/companies/' . $company->id . '/edit');
        $response->assertStatus(200);
    }

    /** @test */
    public function canCreateCompany()
    {
        $this->withExceptionHandling();
        $response = $this->post('/companies', [
            "name" => "sergio",
            "email" => "sergioleon1127@gmail.com"
        ]);
        $response->assertStatus(302);
        $this->assertEquals(1, Company::count());
    }

    /** @test */
    public function canUpdateCompany()
    {
        $this->withExceptionHandling();

        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);
        $response = $this->put('/companies/' . $company->id, [
            'name' => 'company',
            'email' => 'comp@gmail.com',
        ]);
        $this->assertDatabaseHas('companies', [
            'name' => 'company',
            'email' => 'comp@gmail.com',
        ]);
        $response->assertStatus(302);
        $this->assertEquals(1, Company::count());
    }

    /** @test */
    public function canDeleteCompany()
    {
        $this->withExceptionHandling();

        $company = Company::create([
            'name' => 'Test company',
            'email' => 'company@gmail.com',
            'logo' => 'logo.jpg'
        ]);
        $response = $this->delete('/companies/' . $company->id);
        $response->assertStatus(302);
        $this->assertEquals(0, Company::count());
    }
}
