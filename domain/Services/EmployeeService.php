<?php

namespace App\Domain\Services;

use App\Domain\Repositories\EmployeeRepository;

class EmployeeService
{
    private $repository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->repository = $employeeRepository;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function all($parameters = array())
    {
        return $this->repository->all($parameters);
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public  function create($parameters = array())
    {
        return $this->repository->create($parameters);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->repository->show($id);
    }

    /**
     * @param array $parameters
     * @param $id
     * @return mixed
     */
    public function update($parameters = array(), $id)
    {
        return $this->repository->update($parameters, $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }
}
