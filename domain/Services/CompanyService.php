<?php

namespace App\Domain\Services;

use App\Domain\Repositories\CompanyRepository;

class CompanyService
{
    private $repository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->repository = $companyRepository;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function all($parameters = array())
    {
        return $this->repository->all($parameters);
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public  function create($parameters = array())
    {
        return $this->repository->create($parameters);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->repository->show($id);
    }

    /**
     * @param array $parameters
     * @param $id
     * @return mixed
     */
    public function update($parameters = array(), $id)
    {
        return $this->repository->update($parameters, $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }
}
