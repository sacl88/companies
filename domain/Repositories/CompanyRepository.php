<?php

namespace App\Domain\Repositories;

interface CompanyRepository
{
    /**
     * @param $parameters
     * @return mixed
     */
    public function all($parameters = array());

    /**
     * @param array $parameters
     * @return mixed
     */
    public function create($parameters = array());

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);

    /**
     * @param array $parameters
     * @param $id
     * @return mixed
     */
    public function update($parameters = array(), $id);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
